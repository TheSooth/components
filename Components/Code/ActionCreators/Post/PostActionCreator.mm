//
//  PostActionCreator.m
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "PostActionCreator.h"
#import "Story.h"
#import "Dispatcher.h"
#import "ActionTypes.h"

@implementation PostActionCreator

+ (void)likePost:(NSString *)postID
{
    [self performActionOnPostWithID:postID actionType:PostActionTypeLike];
}

+ (void)dislikePost:(NSString *)postID
{
    [self performActionOnPostWithID:postID actionType:PostActionTypeDislike];
}

+ (void)performActionOnPostWithID:(NSString *)postID actionType:(PostActionType)actionType
{
    NSParameterAssert(postID);
    
    [[Dispatcher sharedDispatcher] dispatchAction:{actionType, @{@"postID" : postID}}];
}

@end
