//
//  PostActionCreator.h
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Story;

@interface PostActionCreator : NSObject

+ (void)likePost:(NSString *)postID;
+ (void)dislikePost:(NSString *)postID;

@end
