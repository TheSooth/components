//
//  Dispatcher.h
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Action.h"
#import <ComponentKit/ComponentKit.h>

typedef void(^DispatcherCallback)(const Action &);

@interface Dispatcher : NSObject

+ (instancetype)sharedDispatcher;

- (void)registerCallback:(DispatcherCallback)callBack;
- (void)dispatchAction:(const Action &)payload;

@end
