//
//  Dispatcher.m
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "Dispatcher.h"

static Dispatcher *_dispatcher;

@implementation Dispatcher
{
    NSMutableArray *_callbacks;
}

+ (instancetype)sharedDispatcher
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dispatcher = [Dispatcher new];
    });
    
    return _dispatcher;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _callbacks = [NSMutableArray array];
    }
    return self;
}

- (void)registerCallback:(DispatcherCallback)callBack
{
    [_callbacks addObject:callBack];
}

- (void)dispatchAction:(const Action &)payload
{
    for (DispatcherCallback callback in _callbacks) {
        callback(payload);
    }
}


@end
