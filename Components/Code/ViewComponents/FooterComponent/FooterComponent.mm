//
//  FooterComponent.m
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "FooterComponent.h"
#import "ImageButtonComponent.h"
#import "Story.h"

@implementation FooterComponent
{
    CKComponent *_likeComponent;
}

+ (instancetype)newWithStory:(Story *)story
{
    CKComponentScope scope(self, story.storyID);
    
    CKComponent *likeComponent = [ImageButtonComponent newWithTitles:{
        {UIControlStateNormal, @"Like"},
    } images:{
        {UIControlStateNormal, [UIImage imageNamed:@"th_ufi_like_icon"]},
        {UIControlStateSelected, [UIImage imageNamed:@"th_ufi_like_selected_icon"]}
    } selected:story.isLiked enabled:YES action:{@selector(didTapLikeButton:)}];
    
    CKComponent *commentsComponent = [ImageButtonComponent newWithTitles:{
        {UIControlStateNormal, @"Comment"}
    } images:{
        {UIControlStateNormal, [UIImage imageNamed:@"th_comment_icon"]}
    } selected:NO enabled:YES action:{}];
    
    CKComponent *moreComponent = [ImageButtonComponent newWithTitles:{
        {UIControlStateNormal, @"Share"}
    } images:{
        {UIControlStateNormal, [UIImage imageNamed:@"reply"]}
    } selected:NO enabled:YES action:{}];
    
    CKComponent *buttonsComponent = [CKStackLayoutComponent newWithView:{}
                                                                   size:{}
                                                                  style:{
                                                                      .direction = CKStackLayoutDirectionHorizontal,
                                                                      .alignItems = CKStackLayoutAlignItemsStretch
                                                                  }
                                                               children:{
                                                                   {likeComponent, .alignSelf = CKStackLayoutAlignSelfStretch, .flexGrow = YES},
                                                                   {commentsComponent, .alignSelf = CKStackLayoutAlignSelfStretch, .flexGrow = YES},
                                                                   {moreComponent, .alignSelf = CKStackLayoutAlignSelfStretch, .flexGrow = YES}
                                                               }];
    
    FooterComponent *c = [super newWithComponent:
                          [CKInsetComponent newWithInsets:{.left = 12, .right = 12}
                                                component:
                           [CKStackLayoutComponent newWithView:{}
                                                          size:{}
                                                         style:{.alignItems = CKStackLayoutAlignItemsStretch, .spacing = 8}
                                                      children:{
                                                          {divider()},
                                                          {buttonsComponent}
                                                      }]]];
    
    if (c) {
        c->_likeComponent = likeComponent;
        c->_story = story;
    }
    
    return c;
}


static CKComponent *divider()
{
    return [CKComponent newWithView:{
        [UIView class],
        {{@selector(setBackgroundColor:), [UIColor lightGrayColor]}}
    } size:{.height = 1 / [UIScreen mainScreen].scale }];
}


- (std::vector<CKComponentAnimation>)animationsFromPreviousComponent:(FooterComponent *)previousComponent
{
    if (!self->_story.isLiked && _likeComponent != nil) {
        return {{_likeComponent, scaleToAppear()}}; // Scale the overlay in when it appears.
    } else {
        return {};
    }
}

static CAAnimation *scaleToAppear()
{
    CABasicAnimation *scale = [CABasicAnimation animationWithKeyPath:@"transform"];
    scale.fromValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.0, 0.0, 0.0)];
    scale.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    scale.duration = 0.2;
    return scale;
}



@end
