//
//  FooterComponent.h
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <ComponentKit/ComponentKit.h>
@class Story;

@interface FooterComponent : CKCompositeComponent

@property Story *story;

+ (instancetype)newWithStory:(Story *)story;

@end
