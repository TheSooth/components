//
//  FooterComponentController.m
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "FooterComponentController.h"
#import "Dispatcher.h"
#import <ComponentKit/CKComponentSubclass.h>
#import "FooterComponent.h"
#import "PostsStore.h"
#import "Story.h"
#import "ActionTypes.h"
#import "PostActionCreator.h"

@implementation FooterComponentController
{
    PostsStore *_store;
}

- (void)willMount
{
    [super willMount];
    
    if (!_store) {
        _store = [PostsStore sharedInstance];
    }
}

- (void)didMount
{
    [super didMount];
    [_store addChangeListener:self action:@selector(postDidUpdated:)];
}

- (void)didUnmount
{
    [super didUnmount];
    [_store removeChangeListener:self action:@selector(postDidUpdated:)];
}

- (void)didTapLikeButton:(CKButtonComponent *)sender
{
    if (self.post.isLiked) {
        [PostActionCreator dislikePost:self.post.storyID];
    } else {
        [PostActionCreator likePost:self.post.storyID];
    }
}

- (void)postDidUpdated:(Story *)post
{
    NSLog(@"didUpdatedPost: %@", post);
}

- (Story *)post
{
    return [(FooterComponent *)self.component story];
}

@end

