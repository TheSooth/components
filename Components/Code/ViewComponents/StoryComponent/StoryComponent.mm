//
//  StoryComponent.m
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "StoryComponent.h"
#import "HeaderTitleComponent.h"
#import "StoryImageComponent/StoryImageComponent.h"
#import "StoryDetailsComponent.h"
#import "FooterComponent.h"

@implementation StoryComponent

+ (instancetype)newWithStory:(Story *)story
{
    HeaderTitleComponent *titleComponent = [HeaderTitleComponent newWithStory:story];
    CKComponent *storyDetailsComponent = [CKInsetComponent newWithInsets:{.left = 12} component:
                                          [StoryDetailsComponent newWithStory:story]];
    CKComponent *storyComponent = nil;
    CKComponent *footerComponent = [FooterComponent newWithStory:story];
    
    if (story.storyImage) {
        storyComponent = [StoryImageComponent newWithStory:story];

    }
    
    return [super newWithComponent:
            [CKStackLayoutComponent newWithView:{}
                                           size:{}
                                          style:{.spacing = 8, .alignItems = CKStackLayoutAlignItemsStretch}
                                       children:{
                                           {titleComponent, .spacingBefore = 9},
                                           {storyComponent},
                                           {storyDetailsComponent},
                                           {footerComponent},
                                           {footDivider()}
                                       }]];
}

static CKComponent *footDivider() {
    return [CKComponent newWithView:{
        [UIView class],
        {{@selector(setBackgroundColor:), [UIColor lightGrayColor]}}
    } size:{.height = 5}];
}

@end
