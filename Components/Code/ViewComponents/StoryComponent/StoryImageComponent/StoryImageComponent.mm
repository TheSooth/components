//
//  StoryImageComponent.m
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "StoryImageComponent.h"

@implementation StoryImageComponent

+ (instancetype)newWithStory:(Story *)story
{
    CKComponent *storyDetailsComponent = [CKInsetComponent newWithInsets:{.left = 12}
                                                               component:
                                          [CKLabelComponent newWithLabelAttributes:{.string = story.story}
                                                                    viewAttributes:{}]];
    
    CKComponent *imageComponent = [CKRatioLayoutComponent newWithRatio:0.5 size:{} component:
                                   [CKComponent                                    newWithView:{
        [UIImageView class],
        {
            {@selector(setImage:), story.storyImage},
            {@selector(setContentMode:), @(UIViewContentModeScaleAspectFill)},
            {@selector(setClipsToBounds:), @(YES)}
        }
    } size:{}]
                                   ];
    
    return [super newWithComponent:
            [CKStackLayoutComponent newWithView:{}
                                           size:{}
                                          style:{.direction = CKStackLayoutDirectionVertical, .spacing = 8}
                                       children:{
                                           {storyDetailsComponent},
                                           {imageComponent}
                                       }]];
    
}

@end
