//
//  StoryImageComponent.h
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <ComponentKit/ComponentKit.h>
#import "Story.h"

@interface StoryImageComponent : CKCompositeComponent

+ (instancetype)newWithStory:(Story *)story;

@end
