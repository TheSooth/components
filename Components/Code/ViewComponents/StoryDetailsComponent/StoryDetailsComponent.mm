//
//  StoryDetailsComponentn.m
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "StoryDetailsComponent.h"

@implementation StoryDetailsComponent

+ (instancetype)newWithStory:(Story *)story
{
    UIColor *labelColor = [UIColor lightGrayColor];
    CKComponent *likesComponent = [CKLabelComponent newWithLabelAttributes:{.string = story.likesCountString, .color = labelColor}
                                                            viewAttributes:{}];
    
    CKComponent *commentsComponent = [CKLabelComponent newWithLabelAttributes:{.string = story.commentsCountString, .color = labelColor}
                                                               viewAttributes:{}];
    
    CKComponent *detailsComponent = [CKStackLayoutComponent newWithView:{}
                                                                   size:{}
                                                                  style:{
                                                                      .direction = CKStackLayoutDirectionHorizontal,
                                                                      .spacing = 8,
                                                                  }
                                                               children:{
                                                                   {likesComponent},
                                                                   {commentsComponent}
                                                               }];
    
    return [super newWithComponent:
            [CKStackLayoutComponent newWithView:{}
                                           size:{}
                                          style:{.spacing = 3}
                                       children:{
                                           {detailsComponent}
                                       }]];
}

@end
