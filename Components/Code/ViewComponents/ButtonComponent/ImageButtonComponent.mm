//
//  ButtonComponent.m
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "ImageButtonComponent.h"

@implementation ImageButtonComponent

+ (instancetype)newWithTitles:(const std::unordered_map<UIControlState, NSString *> &)titles
                       images:(const std::unordered_map<UIControlState, UIImage *> &)images
                     selected:(BOOL)selected
                      enabled:(BOOL)enabled
                       action:(CKComponentAction)action
{
    return [super newWithComponent:
            [CKButtonComponent newWithTitles:titles
                                 titleColors:{{UIControlStateNormal, [UIColor lightGrayColor]}}
                                      images:images
                            backgroundImages:{}
                                   titleFont:[UIFont systemFontOfSize:12]
                                    selected:selected
                                     enabled:enabled
                                      action:action
                                        size:{}
                                  attributes:{{@selector(setTitleEdgeInsets:), [NSValue valueWithUIEdgeInsets:UIEdgeInsetsMake(0, 2, 0, 0)]}}
                  accessibilityConfiguration:{}]];
}

@end
