//
//  ButtonComponent.h
//  Components
//
//  Created by TheSooth on 11/18/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <ComponentKit/ComponentKit.h>

@interface ImageButtonComponent : CKCompositeComponent

+ (instancetype)newWithTitles:(const std::unordered_map<UIControlState, NSString *> &)titles
                       images:(const std::unordered_map<UIControlState, UIImage *> &)images
                     selected:(BOOL)selected
                      enabled:(BOOL)enabled
                       action:(CKComponentAction)action;

@end
