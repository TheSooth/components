//
//  HeaderTitleComponent.h
//  Components
//
//  Created by TheSooth on 11/17/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <ComponentKit/ComponentKit.h>
#import "Story.h"
#import "UserProfile.h"

@interface HeaderTitleComponent : CKCompositeComponent

+ (instancetype)newWithStory:(Story *)story;

@end
