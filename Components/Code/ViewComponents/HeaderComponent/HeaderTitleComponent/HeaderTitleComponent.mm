//
//  HeaderTitleComponent.m
//  Components
//
//  Created by TheSooth on 11/17/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "HeaderTitleComponent.h"

@implementation HeaderTitleComponent

+ (instancetype)newWithStory:(Story *)story
{
    CKComponent *storyDetailsComponent = [CKInsetComponent newWithInsets:{.top = 3}
                                                               component:
                                          [CKStackLayoutComponent
                                           newWithView:{}
                                           size:{}
                                           style:{.direction = CKStackLayoutDirectionVertical}
                                           children:{
                                               {[CKTextComponent
                                                 newWithTextAttributes:{
                                                     .attributedString = story.title
                                                 }
                                                 viewAttributes:{
                                                     {@selector(setOpaque:), @NO}
                                                 }
                                                 accessibilityContext:{}],
                                                   .flexShrink = YES
                                               },
                                               {[CKLabelComponent newWithLabelAttributes:{
                                                   .string = story.locationString,
                                                   .color = [UIColor lightGrayColor]
                                               } viewAttributes:{}],
                                                   .flexGrow = YES
                                               }
                                           }
                                           ]];
    
    CKComponent *storyAuthorAvatarComponent = [CKComponent newWithView:{
        [UIImageView class],
        {
            {@selector(setImage:), story.authorProfile.avatar},
            {@selector(setContentMode:), @(UIViewContentModeScaleAspectFit)},
            {@selector(setClipsToBounds:), @(YES)}
        }
    } size:{story.authorProfile.avatar.size.width, story.authorProfile.avatar.size.height}];
    
    CKComponent *chevronComponent = [CKInsetComponent newWithInsets:{.top = 3, 0, 0, 0}
                                                          component:
                                     [CKButtonComponent newWithTitles:{}
                                                          titleColors:{}
                                                               images:{
                                                                   {UIControlStateNormal, [UIImage imageNamed:@"story_chevron"]}
                                                               }
                                                     backgroundImages:{}
                                                            titleFont:{}
                                                             selected:NO
                                                              enabled:YES
                                                               action:nil
                                                                 size:{}
                                                           attributes:{}
                                           accessibilityConfiguration:{}]];
    
    return [super newWithComponent:
            [CKStackLayoutComponent newWithView:{}
                                           size:{}
                                          style:{.direction = CKStackLayoutDirectionHorizontal, .spacing = 8}
                                       children:{
                                           {storyAuthorAvatarComponent, .spacingBefore = 12},
                                           {storyDetailsComponent, .flexShrink = YES, .spacingAfter = 50},
                                           {chevronComponent, .spacingAfter = 12}
                                       }]];
}

@end
