//
//  PostsStore.m
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "PostsStore.h"
#import "ObjectsStorage.h"
#import "Story.h"
#import "Dispatcher.h"
#import "ActionTypes.h"

static PostsStore *_postsStore;

@implementation PostsStore
{
    NSArray *_posts;
    NSMutableDictionary *_listeners;
}

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _postsStore = [PostsStore new];
    });
    
    return _postsStore;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _posts = [ObjectsStorage sharedInstance].getModels;
        _listeners = [NSMutableDictionary dictionary];
        [self registerInDispatcher];
    }
    return self;
}

- (void)registerInDispatcher
{
    [[Dispatcher sharedDispatcher] registerCallback:^(const Action &action) {
        PostAction postAction = (PostAction &)action;
        switch (action.event) {
            case PostActionTypeLike:
                [self likePostWithID:postAction.postID()];
                break;
            case PostActionTypeDislike:
                [self dislikePostWithID:postAction.postID()];
                break;
            default:
                break;
        }
    }];
}

- (void)likePostWithID:(NSString *)postID
{
    Story *post = [self editablePostFromPostWithPostID:postID];
    post.isLiked = YES;
    post.likesCount++;
    
    [self mergePostWithID:postID withPost:post];
}

- (void)dislikePostWithID:(NSString *)postID
{
    Story *post = [self editablePostFromPostWithPostID:postID];
    post.isLiked = NO;
    post.likesCount--;
    
    [self mergePostWithID:postID withPost:post];
}

- (Story *)editablePostFromPostWithPostID:(NSString *)postID
{
    Story *post = [self postWithID:postID];
    return [post copy];
}

- (void)mergePostWithID:(NSString *)postID withPost:(Story *)story
{
    Story *post = [self postWithID:postID];
    NSUInteger postIndex = [_posts indexOfObject:post];
    NSAssert(postIndex != NSNotFound, @"");
    NSMutableArray *posts = [_posts mutableCopy];
    
    [posts replaceObjectAtIndex:postIndex withObject:story];
    _posts = [posts copy];
    
    [self notifyListenersWithPost:story];
}

- (Story *)postWithID:(NSString *)modelID
{
    NSParameterAssert(modelID);
    
    for (Story *story in _posts) {
        if ([story.storyID isEqualToString:modelID]) {
            return story;
        }
    }
    
    @throw [NSException exceptionWithName:modelID reason:@"no model founded" userInfo:nil];
}

- (void)notifyListenersWithPost:(Story *)post
{
    for (NSValue *ownerValue in _listeners.allKeys) {
        id owner = [ownerValue nonretainedObjectValue];
        if (owner) {
            NSString *selectorString = _listeners[ownerValue];
            SEL selector = NSSelectorFromString(selectorString);
            
            [owner performSelector:selector withObject:post];
        }
    }
}

- (void)addChangeListener:(id)owner action:(SEL)action
{
    NSValue *ownerValue = [self valueObjectFromOwner:owner];
    _listeners[ownerValue] = NSStringFromSelector(action);
}

- (void)removeChangeListener:(id)owner action:(SEL)action
{
    NSValue *ownerValue = [self valueObjectFromOwner:owner];
    [_listeners removeObjectForKey:ownerValue];
}

- (NSValue *)valueObjectFromOwner:(id)owner
{
    return [NSValue valueWithNonretainedObject:owner];
}

- (NSString *)ownerKeyFromOwner:(id)owner action:(SEL)action
{
    NSParameterAssert(owner);
    NSParameterAssert(action);
    
    return [NSString stringWithFormat:@"%@:%td-%@", NSStringFromClass(owner), [owner hash], NSStringFromSelector(action)];
}

@end
