//
//  PostsStore.h
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PostsStore : NSObject

@property (readonly) NSArray *posts;

+ (instancetype)sharedInstance;

- (void)likePostWithID:(NSString *)postID;
- (void)dislikePostWithID:(NSString *)postID;

- (void)addChangeListener:(id)owner action:(SEL)action;
- (void)removeChangeListener:(id)owner action:(SEL)action;

@end
