//
//  AppDelegate.h
//  Components
//
//  Created by TheSooth on 11/16/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

