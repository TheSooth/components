//
//  UserProfile.h
//  Components
//
//  Created by TheSooth on 11/17/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfile : NSObject

@property UIImage *avatar;
@property NSString *fullName;
@property NSString *mood;

@end
