//
//  Action.h
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSUInteger ActionEvent;

struct Action {
    ActionEvent event;
    NSDictionary *payload;
};

struct PostAction : Action {
    NSString *postID() const;
};