//
//  Action.m
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "Action.h"


NSString *PostAction::postID() const
{
    if (payload[@"postID"]) {
        return payload[@"postID"];
    }
    
    return @"";
}
