//
//  Story.h
//  Components
//
//  Created by TheSooth on 11/17/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UserProfile;

@interface Story : NSObject <NSCopying>

@property UserProfile *authorProfile;
// Array of strings
@property NSArray *participants;
@property NSString *dateString;
@property NSString *locationString;
@property NSString *story;
@property UIImage *storyImage;

@property NSUInteger likesCount;
@property NSUInteger commentsCount;

@property  BOOL isLiked;

@property (readonly) NSAttributedString *title;
@property (readonly) NSString *likesCountString;
@property (readonly) NSString *commentsCountString;

@property NSString *storyID;


@end
