//
//  Story.m
//  Components
//
//  Created by TheSooth on 11/17/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Story.h"
#import "UserProfile.h"

@implementation Story 

static NSDictionary *boldAttributes()
{
    return @{NSFontAttributeName: [UIFont boldSystemFontOfSize:13]};
}

- (id)copyWithZone:(NSZone *)zone
{
    Story *story = [Story new];
    story.authorProfile = self.authorProfile;
    story.participants = self.participants;
    story.dateString = self.dateString;
    story.locationString = self.locationString;
    story.story = self.story;
    story.storyImage = self.storyImage;
    story.likesCount = self.likesCount;
    story.commentsCount = self.commentsCount;
    story.isLiked = self.isLiked;
    story.storyID = self.storyID;
    
    return story;
}

- (NSAttributedString *)title
{
    NSAttributedString *author = [[NSAttributedString alloc] initWithString:self.authorProfile.fullName attributes:boldAttributes()];
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithAttributedString:author];
    if (self.participants.count > 0) {
        [title appendAttributedString:[[NSAttributedString alloc] initWithString:@" with "]];
        [title appendAttributedString:[self participiantsAttributedString]];
    }
    
    return title;
}

- (NSAttributedString *)participiantsAttributedString
{
    NSMutableArray *participiantsArray = [self.participants mutableCopy];
    NSString *participiant = self.participants.firstObject;
    [participiantsArray removeObject:participiant];
    
    NSMutableAttributedString *participiants = [[NSMutableAttributedString alloc] initWithString:participiant
                                                                                      attributes:boldAttributes()];
    
    if (participiantsArray.count > 0) {
        [participiants appendAttributedString:[[NSAttributedString alloc] initWithString:@" and "]];
        if (participiantsArray.count > 2) {
            NSString *participiantsString = [NSString stringWithFormat:@"%td others.", participiantsArray.count];
            NSMutableAttributedString *otherParticipiantsString = [[NSMutableAttributedString alloc] initWithString:participiantsString
                                                                                                         attributes:boldAttributes()];
            [participiants appendAttributedString:otherParticipiantsString];
        }
    }
    
    return [participiants copy];
}

- (NSString *)likesCountString
{
    return [NSString stringWithFormat:@"%td Likes", self.likesCount];
}

- (NSString *)commentsCountString
{
    return [NSString stringWithFormat:@"%td Comments", self.commentsCount];
}

@end
