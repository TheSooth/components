//
//  ObjectsStorage.m
//  Components
//
//  Created by TheSooth on 11/16/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "ObjectsStorage.h"
#import "UserProfile.h"
#import "Story.h"

@interface ObjectsStorage ()
@property NSArray *models;
@end

static ObjectsStorage *_objectStorage;

@implementation ObjectsStorage

+ (instancetype)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _objectStorage = [ObjectsStorage new];
    });
    
    return _objectStorage;
}

- (NSArray *)getModels
{
    if (!self.models) {
        self.models = [ObjectsStorage generateModels];
    }
    
    return self.models;
}

- (void)likeModel:(Story *)story
{
    Story *storyToModify = nil;
    for (Story *currentStory in self.models) {
        if ([story.storyID isEqualToString:currentStory.storyID]) {
            storyToModify = story;
            break;
        }
    }
    
    NSParameterAssert(storyToModify);
    
    NSMutableArray *models = [self.models mutableCopy];
    Story *tempStory = [storyToModify copy];
    tempStory.authorProfile.fullName = @"John Ive";
    tempStory.isLiked = !tempStory.isLiked;
    tempStory.storyID = [NSUUID UUID].UUIDString;
    
    if (tempStory.isLiked)
        tempStory.likesCount--;
    else
        tempStory.likesCount++;
    
    NSUInteger indexOfModel = [self.models indexOfObject:storyToModify];
    
    [models replaceObjectAtIndex:indexOfModel withObject:tempStory];
    
    self.models = models.copy;
    
    if (self.delegate) {
        [self.delegate updateModelAtIndex:indexOfModel];
    }
}

+ (NSArray *)generateModels
{
    NSInteger objectsCount = arc4random_uniform(40);
    NSMutableArray *models = [NSMutableArray arrayWithCapacity:objectsCount];

    UIImage *avatar = [UIImage imageNamed:@"avatar"];
    UIImage *storyImage = [UIImage imageNamed:@"story_image"];
    UserProfile *profile = [UserProfile new];
    profile.avatar = avatar;
    profile.fullName = @"Ari Grant";
    
    for (NSInteger i = 0; i < objectsCount; i++) {
        Story *story = [Story new];
        story.authorProfile = profile;
        story.participants = @[@"Caitlin Winner", @"Ari Grant", @"Caitlin Winner", @"Andrew Dude", @"bb"];
        story.locationString = @"Yesterday · Menlo Park, CA";
        story.storyImage = storyImage;
        story.story = @"Good friends, good food and a lot of laughs.";
        story.isLiked = rand() % 2;
        if (story.isLiked) {
            story.likesCount = 1;
        }
        story.storyID = [NSUUID UUID].UUIDString;
        
        [models addObject:story];
    }
    
    return models.copy;
}

@end
