//
//  ObjectsStorage.h
//  Components
//
//  Created by TheSooth on 11/16/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Story;

@protocol ObjectsStorageDelegate <NSObject>

- (void)updateModelAtIndex:(NSUInteger)index;

@end

@interface ObjectsStorage : NSObject

@property id <ObjectsStorageDelegate> delegate;

+ (instancetype)sharedInstance;

- (NSArray *)getModels;

- (void)likeModel:(Story *)story;

@end
