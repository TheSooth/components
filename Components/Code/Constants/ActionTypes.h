//
//  ActionTypes.h
//  Components
//
//  Created by TheSooth on 11/19/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

typedef NS_ENUM(NSUInteger, PostActionType) {
    PostActionTypeLike = 1000,
    PostActionTypeDislike
};
