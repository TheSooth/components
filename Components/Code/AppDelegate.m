//
//  AppDelegate.m
//  Components
//
//  Created by TheSooth on 11/16/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "AppDelegate.h"
#import "FeedViewController.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setMinimumInteritemSpacing:0];
    [flowLayout setMinimumLineSpacing:0];
    
    FeedViewController *viewController = [[FeedViewController alloc] initWithCollectionViewLayout:flowLayout];
    
    [_window setRootViewController:[[UINavigationController alloc] initWithRootViewController:viewController]];
    [_window makeKeyAndVisible];
    
    return YES;
}

@end
