//
//  FeedViewController.m
//  Components
//
//  Created by TheSooth on 11/16/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import "FeedViewController.h"
#import "ObjectsStorage.h"
#import "Story.h"
#import <ComponentKit/ComponentKit.h>
#import "HeaderTitleComponent.h"
#import "StoryComponent.h"
#import "PostsStore.h"

@interface FeedViewController () <CKComponentProvider, UICollectionViewDelegateFlowLayout>
@end


@implementation FeedViewController
{
    CKCollectionViewDataSource *_dataSource;
    CKComponentFlexibleSizeRangeProvider *_sizeRangeProvider;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _sizeRangeProvider = [CKComponentFlexibleSizeRangeProvider providerWithFlexibility:CKComponentSizeRangeFlexibleHeight];
    
    _dataSource = [[CKCollectionViewDataSource alloc] initWithCollectionView:self.collectionView
                                                 supplementaryViewDataSource:nil
                                                           componentProvider:self.class
                                                                     context:nil
                                                   cellConfigurationFunction:nil];
    
    CKArrayControllerSections sections;
    sections.insert(0);
    [_dataSource enqueueChangeset:{sections, {}} constrainedSize:{}];
    
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self generateObjects];
}

- (void)generateObjects
{
    NSArray *models = [PostsStore sharedInstance].posts;
    
    __block CKArrayControllerInputItems items;
    [models enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        items.insert([NSIndexPath indexPathForRow:idx inSection:0], models[idx]);
    }];
    
    [_dataSource enqueueChangeset:{{}, items}
                  constrainedSize:[_sizeRangeProvider sizeRangeForBoundingSize:self.collectionView.bounds.size]];
    
    [[PostsStore sharedInstance] addChangeListener:self action:@selector(postDidUpdated:)];
}

- (void)postDidUpdated:(Story *)post
{
    NSInteger postIndex = [[PostsStore sharedInstance].posts indexOfObject:post];
    
    CKArrayControllerInputItems items;
    
    items.update({0, postIndex}, post);
    [_dataSource enqueueChangeset:{{}, items}
                  constrainedSize:[_sizeRangeProvider sizeRangeForBoundingSize:self.collectionView.bounds.size]];
}

+ (CKComponent *)componentForModel:(Story *)model context:(id<NSObject>)context
{
    return [StoryComponent newWithStory:model];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [_dataSource sizeForItemAtIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView
       willDisplayCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [_dataSource announceWillAppearForItemInCell:cell];
}

- (void)collectionView:(UICollectionView *)collectionView
  didEndDisplayingCell:(UICollectionViewCell *)cell
    forItemAtIndexPath:(NSIndexPath *)indexPath
{
    [_dataSource announceDidDisappearForItemInCell:cell];
}

@end
