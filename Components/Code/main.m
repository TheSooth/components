//
//  main.m
//  Components
//
//  Created by TheSooth on 11/16/15.
//  Copyright © 2015 TheSooth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
